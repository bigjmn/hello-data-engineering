from django.db import models
from django.urls import reverse 
# Create your models here.

class UserFave(models.Model):

    user_name = models.CharField(max_length=20)
    user_age = models.IntegerField()

    class Meta:
        ordering = ['-user_name']

    #methods 
    def get_absolute_url(self):
        return reverse('model-detail-view', args=[str(self.id)])
    def __str__(self):
        return self.user_name 
    